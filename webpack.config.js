const package = require('./package.json');
const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');

const PUBLIC_PATH = "";
const DEV = process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'preproduction';
const PREPROD = process.env.NODE_ENV === 'preproduction';

const entry = [ './src/index.js' ];

if (DEV) {
    entry.unshift('./src/dev/index.js');
}

module.exports = {

    entry,

    output: {
        path: path.resolve(__dirname, './dist'),
        publicPath: PUBLIC_PATH,
        filename: `[name].bundle.${package.version}.js`
    },

    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        'scss': 'vue-style-loader!css-loader!sass-loader',
                        'sass': 'vue-style-loader!css-loader!sass-loader',
                        'styl': 'vue-style-loader!css-loader!styl-loader'
                    }
                }
            },
            {
                test: /\.css$/,
                loader: ['style-loader', 'css-loader']
            },
            {
                test: /\.styl$/,
                loader: ['style-loader', 'css-loader', 'stylus-loader']
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpg|gif|svg|ttf|woff|woff2|eot)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]'
                }
            }
        ]
    },

    plugins: [

        new HtmlWebpackPlugin({
            title: 'Baby',
            template: 'src/index.html'
        }),

        new CopyWebpackPlugin([
            { from: 'src/manifest.json' },
            { from: 'server', to: 'server' }
        ]),

        new webpack.DefinePlugin({
            APP_VERSION: JSON.stringify(package.version),
            DEV: JSON.stringify(DEV),
            PREPROD: JSON.stringify(PREPROD)
        })

    ],

    resolve: {
        alias: {
            'src': path.resolve(__dirname, 'src'),
            'component': path.resolve(__dirname, 'src/component'),
            'dao': path.resolve(__dirname, 'src/dao'),
            'model': path.resolve(__dirname, 'src/model'),
            'store': path.resolve(__dirname, 'src/store'),
            'bus': path.resolve(__dirname, 'src/bus'),
            'test': path.resolve(__dirname, 'src/test'),
            'util': path.resolve(__dirname, 'src/util'),
            'view': path.resolve(__dirname, 'src/view'),
            'worker': path.resolve(__dirname, 'src/worker'),
            'vue$': 'vue/dist/vue.js'
        }
    },

    devServer: {
        historyApiFallback: true,
        noInfo: true,
        overlay: true,
        proxy: {
            "/server": "http://localhost"
        }
    },

    performance: {
        hints: false
    }

};

module.exports.devtool = '#source-map';

if (!DEV) {

    module.exports.plugins = (module.exports.plugins || []).concat([
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        })
    ]);

    if (!PREPROD) {
        module.exports.plugins = module.exports.plugins.concat([
            new UglifyJsPlugin({
                sourceMap: true,
                parallel: true,
                uglifyOptions: {
                    ecma: 8
                }
            }),
            new WorkboxPlugin.GenerateSW({
                swDest: 'sw-cache.js',
                runtimeCaching: [{
                    urlPattern: /^https:\/\/fonts\.(?:googleapis|gstatic)\.com\/(.*?)/,
                    handler: 'cacheFirst'
                }]
            })
        ]);
    }

    module.exports.plugins = module.exports.plugins.concat([
        new webpack.LoaderOptionsPlugin({
            minimize: true
        })
    ]);

}