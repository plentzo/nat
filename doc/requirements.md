# Things to Track

 - Everything will track, by default:
    - The time of the event
    - Any notes for the event

## Feedings

 - Breast feeding
    - Which breast?
 - Pumping
    - How many ounces?
    - Duration
 - Bottle feeding
    - How many ounces?

## Spit Ups

 - Timing is most important for gauging frequency

## Diaper Changes

 - Pee or poop
 - Notes for when something looks "off"

## Sleep

 - Location
    - Bassinet
    - Crib
    - Parent
    - Other
 - Duration

## Fussiness

 - Duration
 - Severity (Smiley face range)

## Doctor Appointment

 - General notes
 - Immunizations

## Notables

 - Milestones
    - Starts crawling
    - First step
    - Spoken word
 - Height
 - Weight
 - Pictures! (Link to google photos?)
 - Other random notes




# Functionality

 - Event timing
    - When logging events, assume the current time is the event time, but allow for precise adjusting later
    - Any time the user is asked for a duration, use rough estimates (based on the event), but allow for precise adjusting later
 - Sleep logging...
    - After logging a "sleep start", any other events that aren't "sleep end" are considered interruptions to the sleep
    - Logging any non "sleep end" event will prompt the user to end the sleep duration. (E.g. Sometimes, the baby goes back to sleep after a feeding)
 - Feedings
    - If breast feeding or pumping, keep the most recently used breast prominently on the screen (and suggest using the other)



# Views

 - Dashboard
    - Most recently used breast (if breast feeding or pumping)
    - Sleep schedule
        - Most recent time/duration
        - Average time/duration over past several days
    - Fussiness
        - Frequency over past several days
        - Average severity
        - Worst severity
    - Allow adjusting of "several days" range to something else
 - Calendar
    - Day view
        - Nav at the top for switching days
 - Export
    - Export the log to:
        - PDF (Default)
        - Json
    - Date range:
        - Last month (Default)
        - All time
        - Custom
- Import
    - Import from Json