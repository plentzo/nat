EventBus.$emit('globalAction', { icon: 'check', action() { ... } });
EventBus.$emit('globalMessage', 'message string');
EventBus.$emit('sync', { updated: true, date: DATE })