export function capitalize(str) {
    if (str) {
        return str.match(/(\S+)(\s*)/g)
            .map(p => `${p[0].toUpperCase()}${p.substring(1)}`)
            .join('');
    } else {
        return str;
    }
}

export default {
    capitalize
};
