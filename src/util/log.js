export function debug(name, ...info) {
    if (DEV || PREPROD) {
        console.log(`[${name}]`, ...info.map(i => {
            if ('function' === typeof i) {
                return i();
            } else {
                return i;
            }
        }));
    }
}

export default debug;
