let idIterator = 1;

export function randomId(prefix) {
    return `${prefix || 'js'}-${idIterator += 1}`;
}

export default {
    randomId
};
