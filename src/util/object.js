export function objectContains(src, compare) {
    const srcKeys = new Set(Object.keys(src));
    const entries = Object.entries(compare);
    for (let i = 0, il = entries.length; i < il; i += 1) {
        const [compareKey, compareValue] = entries[i];
        if (srcKeys.has(compareKey)) {
            const result = ('object' === typeof compareValue)
                ? objectContains(src[compareKey], compare[compareKey])
                : compareValue === src[compareKey];
            if (!result) {
                return false;
            }
        }
    }
    return true;
}

export default {
    objectContains
};
