import { debug } from 'util/log';

function handleResponse(response) {

    let resolved = null;

    switch (response.headers.get('Content-type')) {
        case 'application/json':
            resolved = response.json();
            break;
        default:
            resolved = response.text();
            break;
    }

    if (DEV) {
        return resolved.then(value => {
            debug('handleResponse', value);
            return value;
        });
    } else {
        return resolved;
    }

}

/**
 * @param {String} url 
 * @param {Object} data An object to be serialized
 * @param {String} type 'formData' - it will use FormData, 'json' - it will serialize to json.
 */
export function postData(url, data, type = 'formData') {

    debug('postData', url, type, JSON.stringify(data));

    const request = {
        method: 'POST'
    };

    if ('json' === type) {
        request.headers = { 'Content-type': 'application/json' };
        request.body = JSON.stringify(data);
    } else {
        const formData = new FormData();
        Object.entries(data).forEach(([key, value]) => {
            if ('undefined' !== typeof value) {
                let encodedValue;
                if ('object' === typeof value) {
                    encodedValue = JSON.stringify(value);
                } else {
                    encodedValue = value;
                }
                formData.append(key, encodedValue);
            }
        });
        request.body = formData;
    }

    return new Promise(function fetchWrapper(resolve, reject) {
        fetch(url, request)
            .then(response => resolve(handleResponse(response)), reject)
            .catch((e) => {
                debug('postData', 'server unavailable', e);
                reject();
            });
    });

}

export function postJson(url, object) {

    return postData(url, object, 'json');

}

function objectToFormData(obj, form, namespace) {

    var fd = form || new FormData();
    var formKey;

    for (var property in obj) {
        if (obj.hasOwnProperty(property)) {

            if (namespace) {
                formKey = namespace + '[' + property + ']';
            } else {
                formKey = property;
            }

            // if the property is an object, but not a File,
            // use recursivity.
            if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {

                objectToFormData(obj[property], fd, property);

            } else {

                // if it's a string or a File object
                fd.append(formKey, obj[property]);
            }

        }
    }

    return fd;

};