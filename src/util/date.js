import moment from 'moment';

export function getDurationParts(durationInSeconds) {

    const hours = Math.floor(durationInSeconds / 3600.0);
    const minutes = Math.floor((durationInSeconds - hours * 3600) / 60.0);
    const seconds = Math.floor((durationInSeconds - hours * 3600 - minutes * 60));

    return {
        hours,
        minutes,
        seconds
    };

}

export function formatDuration(durationInSeconds) {

    const { hours, minutes, seconds } = getDurationParts(durationInSeconds);

    const result = [];

    if (hours) {
        result.push(hours);
    }

    if (hours && minutes < 10) {
        result.push(`0${minutes}`);
    } else {
        result.push(minutes);
    }

    if (seconds < 10) {
        result.push(`0${seconds}`);
    } else {
        result.push(seconds);
    }

    return result.join(':');

}

/**
 * @param {(Date|String)} date The input date
 * @returns {String} The date formatted as a long-form calendar date, e.g. "January 10, 1986"
 */
export function formatLongDate(date) {
    return moment(date).format('MMMM Do, YYYY');
}

/**
 * @param {(Date|String)} date The input date
 * @returns {String} The date formatted as time
 */
export function formatTime(date) {
    return moment(date).format('h:mma');
}

export function getEndEvent(date, durationInSeconds) {
    return timestamp(moment(date).add(durationInSeconds, 'seconds'));
}

/**
 * @param {(Date|String)} date The input date
 * @returns {Date} The JS Date object for the string
 */
export function parse(date) {
    return moment(date).toDate();
}

export function maxDate(...dates) {
    let result = null;
    for (let i = 0, il = dates.length; i < il; i += 1) {
        if (dates[i]) {
            const date = moment(dates[i]);
            if (!result || date.isAfter(result)) {
                result = date;
            }
        }
    }
    return result;
}

export function minutesSince(date, now = moment()) {

    const resolvedNow = moment(now);
    const then = moment(date);

    return resolvedNow.diff(then, 'minutes');

}

export function secondsSince(date, now = moment()) {

    const resolvedNow = moment(now);
    const then = moment(date);

    return resolvedNow.diff(then, 'seconds');

}

export function timeSince(date, now = moment()) {

    const resolvedNow = moment(now);
    const then = moment(date);

    return formatDuration(resolvedNow.diff(then, 'seconds'));

}

export function timestamp(date = new Date()) {
    return moment(date).valueOf();
}

export function toDateString(date, pattern = 'YYYY-MM-DD') {
    return moment(date).format(pattern);
}

export function toDateTimeString(date, pattern = 'YYYY-MM-DD[T]HH:mm:ss') {
    return moment(date).format(pattern);
}

export function toTimeString(date, pattern = 'HH:mm:ss') {
    return moment(date).format(pattern);
}

export function toPrettyDate(date) {
    const resolvedDate = moment(date);
    const now = moment();
    if (resolvedDate.isSame(now, 'day')) {
        return 'Today';
    } else if (resolvedDate.isSame(now, 'year')) {
        return resolvedDate.format('MMM D');
    } else {
        return resolvedDate.format('MMM D, YYYY');
    }
}

export function toPrettyDateTime(date, timeFormat = 'HH:mm:ss') {

    const result = [];
    const resolvedDate = moment(date);
    const now = moment();

    if (resolvedDate.isSame(now, 'day')) {
        result.push('Today');
    } else if (resolvedDate.isSame(now, 'year')) {
        result.push(resolvedDate.format('MMM D'));
    } else {
        result.push(resolvedDate.format('MMM D, YYYY'));
    }

    result.push(resolvedDate.format(timeFormat));

    return result.join(' ');

}

export function toPrettyTime(date) {
    return moment(date).format('h:mma');
}

/**
 * 
 * @param {(Date|String)} date The input date
 * @returns {String} The full date/time string for the date: YYYY-MM-DDTHH:mm:ss.SSSZ
 */
export function toString(date) {
    return `${toDateString(date)}T${toTimeString(date)}.000Z`;
}

export default {
    formatDuration,
    formatLongDate,
    formatTime,
    getDurationParts,
    getEndEvent,
    parse,
    maxDate,
    minutesSince,
    secondsSince,
    timeSince,
    timestamp,
    toDateString,
    toDateTimeString,
    toString,
    toTimeString,
    toPrettyDate,
    toPrettyDateTime,
    toPrettyTime
};