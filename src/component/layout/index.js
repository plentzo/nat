import PageComponent from './page.vue';

export default Vue => {
    Vue.component('n-page', PageComponent);
}
