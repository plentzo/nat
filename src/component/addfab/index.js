import AddFabComponent from './addfab.vue';

export default Vue => {
    Vue.component('n-addfab', AddFabComponent);
}