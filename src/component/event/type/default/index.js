import DefaultNoteComponent from './note.vue';
import DefaultTimeComponent from './time.vue';

export default Vue => {
    Vue.component('n-event-default-note', DefaultNoteComponent);
    Vue.component('n-event-default-time', DefaultTimeComponent);
};
