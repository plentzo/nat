import DefaultDetailsComponent from './details.vue';
import DefaultIconComponent from './icon.vue';
import DefaultLabelComponent from './label.vue';
import DefaultSummaryComponent from './summary.vue';

export default Vue => {
    Vue.component('n-event-eat-details', DefaultDetailsComponent);
    Vue.component('n-event-eat-icon', DefaultIconComponent);
    Vue.component('n-event-eat-label', DefaultLabelComponent);
    Vue.component('n-event-eat-summary', DefaultSummaryComponent);
};