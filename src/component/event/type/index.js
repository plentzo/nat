import DefaultEventComponents from './default';
import DiaperEventComponents from './diaper';
import EatEventComponents from './eat';
import FussEventComponents from './fuss';
import MessageEventComponents from './message';
import SizeEventComponents from './size';
import SleepEventComponents from './sleep';

export default Vue => {
    Vue.use(DefaultEventComponents);
    Vue.use(DiaperEventComponents);
    Vue.use(EatEventComponents);
    Vue.use(FussEventComponents);
    Vue.use(MessageEventComponents);
    Vue.use(SizeEventComponents);
    Vue.use(SleepEventComponents);
}