import DefaultDetailsComponent from './details.vue';
import DefaultIconComponent from './icon.vue';
import DefaultLabelComponent from './label.vue';
import DefaultSummaryComponent from './summary.vue';

export default Vue => {
    Vue.component('n-event-size-details', DefaultDetailsComponent);
    Vue.component('n-event-size-icon', DefaultIconComponent);
    Vue.component('n-event-size-label', DefaultLabelComponent);
    Vue.component('n-event-size-summary', DefaultSummaryComponent);
};