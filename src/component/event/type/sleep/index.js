import DefaultDetailsComponent from './details.vue';
import DefaultIconComponent from './icon.vue';
import DefaultLabelComponent from './label.vue';
import DefaultSummaryComponent from './summary.vue';

export default Vue => {
    Vue.component('n-event-sleep-details', DefaultDetailsComponent);
    Vue.component('n-event-sleep-icon', DefaultIconComponent);
    Vue.component('n-event-sleep-label', DefaultLabelComponent);
    Vue.component('n-event-sleep-summary', DefaultSummaryComponent);
};