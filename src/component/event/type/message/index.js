import MessageDetailsComponent from './details.vue';
import MessageIconComponent from './icon.vue';
import MessageLabelComponent from './label.vue';
import MessageSummaryComponent from './summary.vue';

export default Vue => {
    Vue.component('n-event-message-details', MessageDetailsComponent);
    Vue.component('n-event-message-icon', MessageIconComponent);
    Vue.component('n-event-message-label', MessageLabelComponent);
    Vue.component('n-event-message-summary', MessageSummaryComponent);
};