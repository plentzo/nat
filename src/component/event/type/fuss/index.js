import DefaultDetailsComponent from './details.vue';
import DefaultIconComponent from './icon.vue';
import DefaultLabelComponent from './label.vue';
import DefaultSummaryComponent from './summary.vue';

export default Vue => {
    Vue.component('n-event-fuss-details', DefaultDetailsComponent);
    Vue.component('n-event-fuss-icon', DefaultIconComponent);
    Vue.component('n-event-fuss-label', DefaultLabelComponent);
    Vue.component('n-event-fuss-summary', DefaultSummaryComponent);
};