import DefaultDetailsComponent from './details.vue';
import DefaultIconComponent from './icon.vue';
import DefaultLabelComponent from './label.vue';
import DefaultSummaryComponent from './summary.vue';

export default Vue => {
    Vue.component('n-event-diaper-details', DefaultDetailsComponent);
    Vue.component('n-event-diaper-icon', DefaultIconComponent);
    Vue.component('n-event-diaper-label', DefaultLabelComponent);
    Vue.component('n-event-diaper-summary', DefaultSummaryComponent);
};