import Event from 'model/event';

import EventComponent from './event.vue';
import EventTypeComponents from './type';

function componentExists(Vue, name) {
    return 'undefined' !== typeof Vue.options.components[name];
}

function findExistingComponent(Vue, ...names) {
    const match = names.find(name => componentExists(Vue, name));
    if (match) {
        return match;
    } else {
        throw Error(`No valid component found in: ${names}`);
    }
}

function createEventWrapperComponent(Vue, wrapperType) {

    Vue.component(`n-event-${wrapperType}`, {
        props: {
            event: Object,
            type: String
        },
        render(h) {

            const resolvedEvent = this.event;

            const targetName = findExistingComponent(Vue,
                `n-event-${resolvedEvent.getType()}-${wrapperType}`,
                `n-event-default-${wrapperType}`
            );
            
            const resolvedProps = Object.assign({}, this.$props);
            resolvedProps.event = resolvedEvent;

            return h(
                targetName, {
                    attrs: this.$attrs,
                    on: this.$listeners,
                    props: resolvedProps
                },
                [ this.$slots.default ]
            );
        }
    });
}

export default (Vue, router) => {

    Vue.use(EventTypeComponents);

    Vue.component('n-event', EventComponent);

    [
        'details',
        'icon',
        'label',
        'note',
        'summary',
        'time'
    ].forEach(wrapperType => createEventWrapperComponent(Vue, wrapperType));

}
