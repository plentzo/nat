import EventsComponent from './events.vue';

export default Vue => {
    Vue.component('n-events', EventsComponent);
};
