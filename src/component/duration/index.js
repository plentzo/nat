import DurationComponent from './duration.vue';

export default Vue => {
    Vue.component('n-duration', DurationComponent);
}
