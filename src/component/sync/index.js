import DateUtil from 'util/date';
import EventBus from 'bus';
import moment from 'moment';
import md5 from 'md5';
import { debug } from 'util/log';
import { reduceToHashesMap, reduceToEventsMap } from 'model/Events';

const EMPTY_ARRAY_HASH = md5('[]');

export function sync(store, dao) {

    return new Promise(async (resolve, reject) => {

        const { user, baby, lastSyncDate, lastSyncHash } = store.getters;
        const localEvents = store.getters.events;
        const { userId } = user;
        const { babyId } = baby;
        const localEventsHash = localEvents.hash;
        let serverEventsHash = '';

        try {
            serverEventsHash = await dao.getLatestEventsHash(userId, babyId);
        } catch (e) {
            console.warn('Server is not available', e);
            resolve(false);
        }

        debug('sync', { lastSyncDate, lastSyncHash, serverEventsHash, localEventsHash });

        if (lastSyncHash === serverEventsHash && lastSyncHash === localEventsHash && EMPTY_ARRAY_HASH !== serverEventsHash) {

            debug('sync', 'nothing to sync');
            store.commit('setLastSync');

        } else {

            const serverEvents = await dao.getEvents(userId, babyId);
            const lastSyncEvents = resolveLastSyncEvents(store, serverEvents);
            const mergedEvents = mergeEvents(localEvents, serverEvents, lastSyncEvents);

            if (serverEventsHash !== mergedEvents.hash) {
                await dao.updateEvents(user.userId, baby.babyId, mergedEvents);
            }

            if (serverEventsHash !== localEventsHash) {
                // Announce
                EventBus.$emit('globalMessage', 'Synced');
            }

            // Broadcast
            EventBus.$emit('sync', {});
            store.commit('setEvents', mergedEvents);
            store.commit('setLastSync', mergedEvents);

        }

        resolve(true);

    });

}

function resolveLastSyncEvents(store, serverEvents) {
    let lastSyncEvents = store.getters.lastSyncEvents;
    if (lastSyncEvents && Object.keys(lastSyncEvents).length > 0) {
        return lastSyncEvents;
    } else {
        return reduceToHashesMap(serverEvents);
    }
}

/**
 * @param {Events} localEvents Events in local storage
 * @param {Events} serverEvents Events from the server
 * @param {Array} lastSyncEvents Events stored after the last sync/merge
 */
function mergeEvents(localEvents, serverEvents, lastSyncEvents) {

    const localEventsById = reduceToEventsMap(localEvents);
    const serverEventsById = reduceToEventsMap(serverEvents);

    // The event is added if it doesn't have a local version
    const addedServerEvents = serverEvents.filter(serverEvent => !localEventsById[serverEvent.eventId]);

    // Events that were here in the last sync, but the hash has changed
    const updatedServerEvents = serverEvents.filter(serverEvent => {
        const localEvent = localEventsById[serverEvent.eventId];
        // The event is "updated" if the local version is older
        return localEvent && localEvent.updated < serverEvent.updated;
    });

    // TODO Offer a way to merge/resolve conflicts if there's a newer version in local AND
    // server events (compared to the last sync).

    debug('sync.mergeEvents', () => ({
        addedServerEvents: addedServerEvents.map(e => ({ eventId: e.eventId, hash: e.hash })),
        updatedServerEvents: updatedServerEvents.map(e => ({ eventId: e.eventId, hash: e.hash }))
    }));

    // Add in local
    localEvents.add(null, addedServerEvents);

    // Update in local
    localEvents.update(updatedServerEvents);

    return localEvents;

}

export default {
    install(Vue) {

    },
    sync
};
