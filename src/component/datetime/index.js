import DatetimeComponent from './datetime.vue';

export default Vue => {
    Vue.component('n-datetime', DatetimeComponent);
}