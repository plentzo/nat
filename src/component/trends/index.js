import TrendsComponent from './trends.vue';

export default Vue => {
    Vue.component('n-trends', TrendsComponent);
};
