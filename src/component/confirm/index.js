import ConfirmComponent from './confirm.vue';

export default Vue => {
    Vue.component('n-confirm', ConfirmComponent);
};
