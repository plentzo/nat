import ChartComponent from './chart.vue';

export default Vue => {
    Vue.component('n-chart', ChartComponent);
};
