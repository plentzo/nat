import AddfabComponents from './addfab';
import ChartComponents from './chart';
import ConfirmComponents from './confirm';
import DatetimeComponents from './datetime';
import DurationComponents from './duration';
import EventComponents from './event';
import EventsComponents from './events';
import LayoutComponents from './layout';
import SyncComponents from './sync';
import TrendComponents from './trends';

const components = [
    AddfabComponents,
    ChartComponents,
	ConfirmComponents,
    DatetimeComponents,
    DurationComponents,
    EventComponents,
    EventsComponents,
    LayoutComponents,
    SyncComponents,
    TrendComponents
];

export default Vue => {
    components.forEach(component => Vue.use(component));
}
