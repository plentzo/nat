import gzip from 'gzip-js';
import Baby from 'model/Baby';
import Event from 'model/event';
import Events from 'model/Events';
import Settings from 'model/Settings';
import User from 'model/User';
import DateUtil from 'util/date';
import { debug } from 'util/log';
import { postData, postJson } from 'util/ajax';

function bufferToBase64(buffer) {
    const binary = [];
    const bytes = new Uint8Array(buffer);
    for (let i = 0, il = bytes.byteLength; i < il; i += 1) {
        binary.push(String.fromCharCode(bytes[i]));
    }
    return window.btoa(binary.join(''));
}

function gzipObject(events) {
    return bufferToBase64(gzip.zip(JSON.stringify(events), {}));
}

class Dao {

    createBaby(userId, baby) {
        return postJson('server/createBaby.php', { userId, baby })
            // Returns the baby
            .then(baby => new Baby(baby));
    }

    getBaby(userId, babyId) {
        return postData('server/getBaby.php', { userId, babyId })
            // Returns an array of Baby objects
            .then(baby => new Baby(baby));
    }

    getEvents(userId, babyId) {
        return postData('server/getEvents.php', { userId, babyId })
            // Returns an array of Baby objects
            .then(events => new Events(events));
    }

    getLatestEventsHash(userId, babyId) {
        return postData('server/getLatestEventsHash.php', { userId, babyId })
            // Returns the hash (or null)
            .then(hash => hash);
    }

    getUser(token) {
        return postData('server/getUser.php', { token })
            // Returns a User object
            .then(user => new User(user));
    }

    /**
     * @returns {Promise} Resolves to true if the server is reachable, false otherwise.
     */
    pingServer() {
        return postData('server/ping.php', {})
            .then(() => true, () => false);
    }

    updateEvents(userId, babyId, events) {
        return postJson('server/updateEvents.php', { userId, babyId, hash: events.hash, events: gzipObject(events) })
            // Returns true/false for whether it updated/not
            .then(result => Boolean(result));
    }

    updateSettings(userId, settings) {
        return postJson('server/updateSettings.php', { userId, settings })
            // Returns the updated settings
            .then(settings => new Settings(settings));
    }

    updateUser(userId, user) {
        return postJson('server/updateUser.php', { userId, user })
            // Returns the updated user
            .then(user => new User(user));
    }

}

export default Vue => {

    const dao = new Dao();

    Vue.use({
        install(Vue) {
            Object.defineProperty(Vue.prototype, '$dao', {
                get() {
                    return dao;
                }
            });
        }
    });

    return dao;

};