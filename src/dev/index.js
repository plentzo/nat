import fetchMock from 'fetch-mock';
import moment from 'moment';

import DateUtil from 'util/date';
import Baby from 'model/Baby';
import Events from 'model/Events';
import EatEvent from 'model/event/EatEvent';
import SizeEvent from 'model/event/SizeEvent';
import SleepEvent from 'model/event/SleepEvent';
import User from 'model/User';

import { debug } from 'util/log';

function pastDate(hours = 1, minutes = 0) {
    return DateUtil.toDateTimeString(moment().subtract(hours, 'hours').subtract(minutes, 'minutes'));
}

function futureDate(hours = 1, minutes = 0) {
    return DateUtil.toDateTimeString(moment().add(hours, 'hours').add(minutes, 'minutes'));
}

function formDataToJson(formData) {
    const obj = {};
    formData.forEach((v, k) => obj[k] = v);
    return obj;
}

const testUser = new User({
    userId: 1,
    name: 'Test User',
    email: 'test@plentz.net',
    settings: {},
    babies: [ 1 ]
});

const testBaby = new Baby({
    babyId: 1,
    name: 'Test Baby',
    gender: 'boy'
});

const testEvents = new Events();

testEvents.add(1, new SizeEvent({ date: pastDate(500, 0), duration: 2882, details: { height: 20, weight: 72 } }),);
testEvents.add(1, new SizeEvent({ date: pastDate(450, 0), duration: 2882, details: { height: 22, weight: 76 } }),);
testEvents.add(1, new SizeEvent({ date: pastDate(402, 0), duration: 2882, details: { height: 25, weight: 80 } }),);
testEvents.add(1, new SizeEvent({ date: pastDate(300, 0), duration: 2882, details: { height: 30, weight: 102 } }),);
testEvents.add(1, new SleepEvent({ date: pastDate(6, 0), duration: 2882, details: { location: 'Bassinet' } }),);
testEvents.add(1, new EatEvent({ date: pastDate(3, 0), duration: 300, details: { type: 'breast', breast: 'left' } }),);
testEvents.add(1, new EatEvent({ date: pastDate(2, 30), duration: 344, note: 'Alright', details: { type: 'breast', breast: 'right' } }),);
testEvents.add(1, new EatEvent({ date: pastDate(0, 16), details: { type: 'breast', breast: 'left' } }));

// Mock the store

// Store.commit('setUser', testUser);

// Mock the requests

fetchMock.post('server/createBaby.php', (url, res) => {
    const { baby } = JSON.parse(res.body);
    baby.babyId = Math.floor(Math.random() * 9999999);
    return { headers: { 'Content-type': 'application/json' }, body: JSON.stringify(baby) };
});

fetchMock.post('server/createEvent.php', (url, res) => {
    const { event } = JSON.parse(res.body);
    event.eventId = Math.floor(Math.random() * 9999999);
    return { headers: { 'Content-type': 'application/json' }, body: JSON.stringify(event) };
});

fetchMock.post('server/deleteEvent.php', (url, res) => {
    const { eventId } = JSON.parse(res.body);
    return { headers: { 'Content-type': 'application/json' }, body: eventId };
});

fetchMock.post('server/updateEvent.php', (url, res) => {
    const { event } = JSON.parse(res.body);
    return { headers: { 'Content-type': 'application/json' }, body: event };
});

fetchMock.post('server/updateSettings.php', (url, res) => {
    const { settings } = JSON.parse(res.body);
    return { headers: { 'Content-type': 'application/json' }, body: settings };
});

fetchMock.post('server/getBaby.php', testBaby);
fetchMock.post('server/getEvents.php', testEvents);
fetchMock.post('server/getEventLog.php', []);
fetchMock.post('server/getUser.php', testUser);

fetchMock.post('*', (url, opts) => {
    return {};
});
