import sinon from 'sinon';

const RESPONSE_OK = 200;

class ResponseBuilder {

    constructor(server, type, url) {
        this.server = server;
        this.type = type;
        this.url = url;
    }

    send(text, headers = {}) {
        return this.server.respondWith(
            this.type.toUpperCase(),
            this.url,
            [ RESPONSE_OK, headers, text ]
        )
    }

    sendJson(json, headers = {}) {
        const jsonObject = 'string' === typeof json ? json : JSON.stringify(json);
        return this.send(jsonObject, Object.assign({ 'Content-Type': 'application/json' }));
    }

}

export default class MockServer {

    constructor() {
        this.server = null;
    }

    create() {
        this.server = sinon.fakeServer.create();
        this.server.respondImmediately = true;
    }

    get requests() {
        return this.server.requests();
    }

    restore() {
        this.server.restore();
    }

    wait() {
        return assertJSXAttribute.waitForActiveRequests();
    }

    when(type, url) {
        return new ResponseBuilder(this.server, type, url);
    }

}