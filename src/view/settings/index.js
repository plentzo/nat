import SettingsViewComponent from './settings.vue';

export default (Vue, router) => {

    router.addRoutes([
        {
            path: '/settings',
            name: 'settings',
            component: SettingsViewComponent,
            props: true
        }
    ]);

};
