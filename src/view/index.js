import BabyViewComponents from './baby';
import BrowseViewComponents from './browse';
import DashboardViewComponents from './dashboard';
import EventViewComponents from './event';
import ExportViewComponents from './export';
import LoginViewComponents from './login';
import SettingsViewComponents from './settings';
import TimelineViewComponents from './timeline';

const components = [
    BabyViewComponents,
    BrowseViewComponents,
    DashboardViewComponents,
    EventViewComponents,
    ExportViewComponents,
    LoginViewComponents,
    SettingsViewComponents,
    TimelineViewComponents
];

export default (Vue, router) => {
    components.forEach(component => Vue.use(component, router));
}
