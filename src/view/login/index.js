import LoginButtonComponent from './loginButton.vue';
import LoginComponent from './login.vue';
import User from 'model/User';

export default (Vue, router) => {

    Vue.component('n-login-btn', LoginButtonComponent);
    Vue.component('n-login', LoginComponent);

    router.addRoutes([
        {
            path: '/login',
            name: 'login',
            component: LoginComponent
        }
    ]);

}