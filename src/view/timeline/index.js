import TimelineViewComponent from './timeline.vue';

export default (Vue, router) => {

    router.addRoutes([
        {
            path: '/timeline/:date?',
            name: 'timeline',
            component: TimelineViewComponent,
            props: true
        }
    ]);

};
