import EventCreateComponents from './create';
import EventEditComponents from './edit';


export default (Vue, router) => {

    EventCreateComponents(Vue, router);
    EventEditComponents(Vue, router);

};