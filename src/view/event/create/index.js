import DashboardViewComponent from './dashboard.vue';
import FormViewComponent from './form.vue';

export default (Vue, router) => {

    router.addRoutes([
        {
            path: '/event/create/:date?',
            name: 'event/create',
            component: DashboardViewComponent,
            props: true
        }
    ]);

    router.addRoutes([
        {
            path: '/event/create/:date/:type',
            name: 'event/create/type',
            component: FormViewComponent,
            props: true
        }
    ]);

}