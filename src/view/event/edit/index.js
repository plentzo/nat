import FormViewComponent from './form.vue';

export default (Vue, router) => {

    router.addRoutes([
        {
            path: '/event/edit/:eventId',
            name: 'event/edit',
            component: FormViewComponent,
            props: true
        }
    ]);

}