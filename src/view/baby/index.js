import BabyCreateComponents from './create';

export default (Vue, router) => {

    Vue.use(BabyCreateComponents, router);

};
