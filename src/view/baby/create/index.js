import FormViewComponent from './form.vue';

export default (Vue, router) => {

    router.addRoutes([
        {
            path: '/baby/create/:method?',
            name: 'baby/create',
            component: FormViewComponent,
            props: true
        }
    ]);

}