import ExportViewComponent from './export.vue';

export default (Vue, router) => {

    router.addRoutes([
        {
            path: '/export',
            name: 'export',
            component: ExportViewComponent,
            props: true
        }
    ]);

};
