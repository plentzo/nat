import DashboardComponent from './dashboard.vue';

export default (Vue, router) => {

    router.addRoutes([
        {
            path: '/dashboard',
            name: 'dashboard',
            component: DashboardComponent
        }
    ]);

};
