import BrowseComponent from './browse.vue';

export default (Vue, router) => {

    router.addRoutes([
        {
            path: '/browse/:filter',
            name: 'browse',
            component: BrowseComponent,
            props(route) {
                return {
                    filter: JSON.parse(route.params.filter)
                }
            }
        }
    ]);

};
