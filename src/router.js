import VueRouter from 'vue-router';
import DateUtil from 'util/date';
import { sync } from 'component/sync';

async function checkState(store, dao, to, from, next) {
    if (!store.state.authenticated) {
        // The user needs to authenticate
        if ('login' !== to.name) {
            // And they're not headed to the login page already
            store.state.loginFromRoute = to.path;
            return next({ name: 'login', replace: true });
        }
    } else if (!store.state.user.babies.length) {
        // The user's authenticated, but doesn't have any babies
        if ('baby/create' !== to.name) {
            // And they're not headed to the baby create page
            return next({ name: 'baby/create', replace: true });
        }
    } else if (!store.getters.baby) {
        // The user is authenticated and has registered babies, but one isn't chosen yet
        const babies = store.state.user.babies;
        const [firstBaby] = babies;
        const baby = await dao.getBaby(store.state.user.userId, firstBaby);
        const events = await dao.getEvents(store.state.user.userId, firstBaby);
        store.commit('setBaby', baby);
        sync(store, dao);
    }
    return next();
}

export default (Vue, store, dao) => {

    const router = new VueRouter();

    Vue.use(VueRouter);

    // Default route
    router.addRoutes([
        {
            path: '/',
            name: 'home',
            redirect: { name: 'dashboard' }
        }
    ]);

    router.beforeEach((to, from, next) => checkState(store, dao, to, from, next));

    return router;

}