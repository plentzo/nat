import { debug } from 'util/log';

function getLocalStorageKey(name) {
    return `nat.${name}`;
}

export const key = {

    baby(userId, babyId) {
        return `baby?user=${userId}&baby=${babyId}`;
    },

    events(userId, babyId) {
        return `events?user=${userId}&baby=${babyId}`;
    },

    /**
     * Stores a list of { eventId, hash }
     */
    lastSyncEvents() {
        return 'lastSyncEvents'
    },

    lastSyncHash() {
        return 'lastSyncHash';
    },

    lastSyncDate() {
        return 'lastSyncDate';
    },

    selectedBabyId(userId) {
        return `selectedBabyId?user=${userId}`;
    },

    user() {
        return 'user';
    },

    version() {
        return 'version';
    }

};

export function clear() {

    localStorage.clear();

}

export function load(name, defaultValueFn) {

    const key = getLocalStorageKey(name);
    const data = localStorage.getItem(key);

    debug('local.load', name, data ? data.length : 'no data found');

    if (data) {
        return JSON.parse(data);
    } else if ('function' === typeof defaultValueFn) {
        save(name, defaultValueFn(name));
        return load(name);
    } else {
        return null;
    }

}

export function remove(name) {

    const key = getLocalStorageKey(name);

    debug('local.remove', name);

    localStorage.removeItem(key);

}

export function save(name, value) {

    const key = getLocalStorageKey(name);
    const data = JSON.stringify(value);

    debug('local.save', name, data.length);

    localStorage.setItem(key, JSON.stringify(value));

    return true;

}

export default {
    clear,
    key,
    load,
    remove,
    save
};
