import Vuex from 'vuex';
import moment from 'moment';
import lang from '../lang';
import Local from './local';

import Baby from 'model/Baby';
import Event from 'model/event';
import Events from 'model/Events';
import Settings from 'model/Settings';

import DateUtil from 'util/date';
import { debug } from 'util/log';
import { sync } from 'component/sync';

function getFullTitle(state) {
    
    const fullTitle = [ lang.appTitle ];

    if (state.baby) {
        fullTitle.push(state.baby.name);
    }

    if (state.title) {
        fullTitle.push(state.title);
    }

    return fullTitle.join(' - ');

}

const store = {
   
    state() {

        // Validate the local state first
        const currentVersion = Local.load(Local.key.version());
        if (currentVersion && currentVersion !== APP_VERSION) {
            debug('store.validateVersion', 'version mismatch');
            Local.clear();
        }
        Local.save(Local.key.version(), APP_VERSION);

        const user = Local.load(Local.key.user());
        const authenticated = Boolean(user);
        let baby = null;
        let events = new Events();
        let settings = new Settings();
        const lastSyncEvents = Local.load(Local.key.lastSyncEvents(), () => ({}));
        const lastSyncDate = Local.load(Local.key.lastSyncDate(), () => '');
        const lastSyncHash = Local.load(Local.key.lastSyncHash(), () => '');
        const title = lang.appTitle;

        if (authenticated && user) {
            const selectedBabyId = Local.load(Local.key.selectedBabyId(user.userId));
            settings = new Settings(user.settings);
            if (selectedBabyId) {
                baby = new Baby(Local.load(Local.key.baby(user.userId, selectedBabyId)));
                if (baby) {
                    events = new Events(Local.load(Local.key.events(user.userId, baby.babyId), () => []));
                }
            }
        }

        return {
            authenticated,
            baby,
            events,
            lastSyncEvents,
            lastSyncDate,
            lastSyncHash,
            settings,
            title,
            user
        };

    },

    getters: {
        baby(state, getters) {
            return state.baby;
        },
        eventById(state, getters) {
            return (eventId) => {
                const result = getters.events.filter(event => eventId === event.eventId);
                return result.length ? result[0] : null;
            };
        },
        events(state) {
            return state.events;
        },
        fullTitle(state) {
            return getFullTitle(state);
        },
        lastSyncEvents(state) {
            return state.lastSyncEvents;
        },
        lastSyncDate(state) {
            return state.lastSyncDate;
        },
        lastSyncHash(state) {
            return state.lastSyncHash;
        },
        title(state) {
            return state.baby;
        },
        user(state) {
            return state.user;
        }
    },

    actions: {

        createEvent({ state }, { event }) {
            const { user, baby } = state;
            if (user && baby && event) {
                const resolvedEvent = state.events.add(user.userId, event);
                Local.save(Local.key.events(user.userId, baby.babyId), state.events);
                return resolvedEvent;
            } else {
                throw Error('Commit createEvent error: event undefined');
            }
        },

        deleteEvent({ state }, { event }) {
            const { user, baby } = state;
            if (user && baby && event) {
                state.events.remove(event);
                Local.save(Local.key.events(user.userId, baby.babyId), state.events);
            }
        },

        updateEvent({ state }, { eventId, event, log = false }) {
            const { user, baby } = state;
            if (user && baby && event) {
                state.events.update(event);
                Local.save(Local.key.events(user.userId, baby.babyId), state.events);
            }
        }

    },

    mutations: {

        addEvent(state, event) {
            const { user, baby } = state;
            if (user && baby && event) {
                const resolvedEvent = state.events.add(user.userId, event);
                Local.save(Local.key.events(user.userId, baby.babyId), state.events);
            }
        },

        createBaby(state, baby) {
            if (baby) {
                state.user.babies.push(baby.babyId);
                Local.save(Local.key.user(), state.user);
                Local.save(Local.key.baby(state.user.userId, baby.babyId), baby);
            } else {
                throw Error('Commit createBaby error: baby undefined');
            }
        },

        deleteEvent(state, eventId) {
            const { user, baby } = state;
            if (user && baby && eventId) {
                this.events.remove(eventId);
                Local.save(Local.key.events(state.user.userId, state.baby.babyId), state.events);
            }
        },

        evictAll(state) {
            // Evict everything but nat.user
            Local.clear();
        },

        setBaby(state, baby) {
            if (baby) {
                state.baby = baby;
                Local.save(Local.key.selectedBabyId(state.user.userId), baby.babyId);
                Local.save(Local.key.baby(state.user.userId, state.baby.babyId), baby);
            } else {
                throw Error('Commit setBaby error: baby undefined');
            }
        },

        setEvents(state, events) {
            if (events) {
                state.events = new Events(events);
                Local.save(Local.key.events(state.user.userId, state.baby.babyId), events);
            } else {
                throw Error('Commit setEvents error: events undefined');
            }
        },

        /**
         * @param {State} state 
         * @param {Events} serverEvents The events pulled from the server during sync. We'll get the hash and eventIds from here.
         */
        setLastSync(state, serverEvents) {

            const date = DateUtil.timestamp();
            state.lastSyncDate = date;
            Local.save(Local.key.lastSyncDate(), state.lastSyncDate);

            if (serverEvents) {
                // Update the last sync info with known events
                const { hash } = serverEvents;
                const events = serverEvents.map(event => ({ eventId: event.eventId, hash: event.hash }));
                debug('setLastSync', hash, date, events);
                state.lastSyncEvents = events;
                state.lastSyncHash = hash;
                Local.save(Local.key.lastSyncEvents(), state.lastSyncEvents);
                Local.save(Local.key.lastSyncHash(), state.lastSyncHash);
            }

        },

        setTitle(state, title) {
            state.title = title;
            document.title = getFullTitle(state);
        },

        setUser(state, user) {
            if (user) {
                state.user = user;
                state.authenticated = Boolean(user);
                if (state.authenticated) {
                    Local.save(Local.key.user(), user);
                }
            } else {
                throw Error('Commit setUser error: user undefined');
            }
        },

        updateBaby(state, baby) {
            if (baby) {
                state.baby = baby;
                Local.save(Local.key.baby(state.user.userId, baby.babyId), baby);
            } else {
                throw Error('Commit updateBaby error: baby undefined');
            }
        },

        updateSettings(state, settings) {
            if (settings) {
                state.settings = settings;
                state.user.settings = settings;
                Local.save(Local.key.user(), state.user);
            } else {
                throw Error('Commit updateSettings error: settings undefined');
            }
        },

        updateEvent(state, event) {
            const { user, baby } = state;
            if (user && baby && event) {
                state.events.update(event);
                Local.save(Local.key.events(user.userId, baby.babyId), state.events);
            } else {
                throw Error('Commit updateEvent error: event undefined');
            }
        }

    }

};

export default function init(Vue) {

    Vue.use(Vuex);
    return new Vuex.Store(store);
    
}
