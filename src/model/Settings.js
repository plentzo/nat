import DateUtil from '../util/date';

export default class Settings {

    constructor({ theme = 'default', dark = false } = {}) {
        this.theme = theme;
        this.dark = dark;
    }

}

