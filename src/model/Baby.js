export default class Baby {

    constructor({ babyId, name, gender } = {}) {
        this.babyId = babyId;
        this.name = name;
        this.gender = gender;
    }

}
