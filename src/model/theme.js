const DEFAULT_COLORS = {
    primary: '#00695C',
    secondary: '#00897B',
    accent: '#E65100',
    error: '#ff5252',
    info: '#2196f3',
    success: '#4caf50',
    warning: '#ffc107'
};

class Theme {

    constructor({
        name,
        colors
    }) {
        this.name = name;
        this.colors = Object.assign({}, DEFAULT_COLORS, colors);
    }

    static getPreset(name) {
        return Theme.preset[name] || Theme.preset.default;
    }

}

Theme.preset = {
    default: new Theme({ name: 'Default' }),
    boy: new Theme({ name: 'Boy', colors: { primary: '#0277bd', secondary: '#039be5', accent: '#ef6c00' }})
};

export default Theme;
