import Settings from 'model/Settings';

export default class User {

    /**
     * @param {Object} config
     * @param {Array} config.babies An array of ints (baby IDs)
     */
    constructor({ userId, name, email, imageUrl, settings = {}, babies = [] } = {}) {
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.imageUrl = imageUrl;
        this.settings = new Settings('string' === typeof settings ? JSON.parse(settings) : settings);
        this.babies = babies;
    }

}
