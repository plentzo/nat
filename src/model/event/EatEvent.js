import DefaultEvent from 'model/event/DefaultEvent';

export const TYPES = [
    'breast',
    'bottle',
    'pump'
];

export const BREAST_LOCATIONS = [
    'Left',
    'Left then Right',
    'Right then Left',
    'Right'
];

export const BOTTLE_AMOUNTS = [
    '< 1oz',
    '1oz',
    '2oz',
    '3oz',
    '4oz',
    '5oz',
    '6oz',
    '7oz',
    '8oz',
    '9oz',
    '> 9oz'
];

export const TYPE = "eat";

export default class EatEvent extends DefaultEvent {

    /**
     * @param {Object} props Props as described in DefaultEvent
     * @param {Object} props.details The details for this event
     * @param {String} props.details.type "breast" or "bottle"
     * @param {String} props.details.breast "left" or "right"
     * @param {Number} props.details.amount
     */
    constructor(props) {

        const {
            type,
            breast,
            amount
        } = props.details || {};

        super(TYPE, true, props, { type, breast, amount });

    }

}

EatEvent.type = TYPE;
