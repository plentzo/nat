import DefaultEvent from 'model/event/DefaultEvent';

export const TYPE = "message";

export default class MessageEvent extends DefaultEvent {

    /**
     * @param {Object} props Props as described in DefaultEvent
     */
    constructor(props) {

        super(TYPE, false, props, {});

    }

}

MessageEvent.type = TYPE;
