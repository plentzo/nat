import DefaultEvent from 'model/event/DefaultEvent';

export const TYPE = "size";

export default class SizeEvent extends DefaultEvent {

    /**
     * @param {Object} props Props as described in DefaultEvent
     * @param {Object} props.details The details for this event
     * @param {Number} props.details.height Height in inches
     * @param {Number} props.details.weight Weight in ounces
     */
    constructor(props) {

        const {
            height,
            weight
        } = props.details || {};

        super(TYPE, false, props, { height, weight });

    }

    getHeightFeet() {
        return Math.floor(this.details.height / 12);
    }

    getHeightInches() {
        return this.details.height - this.getHeightFeet() * 12;
    }

    getWeightPounds() {
        return Math.floor(this.details.weight / 16);
    }

    getWeightOunces() {
        return this.details.weight - this.getHeightFeet() * 16;
    }

    /**
     * Get the formatted height
     * @return {String} The height represented as X' Y"
     */
    getFormattedHeight() {

        const feet = this.getHeightFeet();
        const inches = this.getHeightInches();
        const result = [];

        if (feet > 0) {
            result.push(`${feet}'`);
        }

        if (inches > 0) {
            result.push(`${inches}"`)
        }

        return result.join(' ');

    }

    /**
     * Get the formatted weight
     * @return {String} The weight represented as Xlb Yoz
     */
    getFormattedWeight() {

        const pounds = Math.floor(this.details.weight / 16);
        const ounces = this.details.weight - pounds * 16;
        const result = [];

        if (pounds > 0) {
            result.push(`${pounds}lb`);
        }

        if (ounces > 0) {
            result.push(`${ounces}oz`)
        }

        return result.join(' ');

    }

    setHeight(feet, inches) {
        this.details.height = parseInt(feet) * 12 + parseInt(inches);
    }

    setWeight(pounds, ounces) {
        this.details.weight = parseInt(pounds) * 16 + parseInt(ounces);
    }

}

SizeEvent.type = TYPE;
