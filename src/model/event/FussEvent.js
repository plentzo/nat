import DefaultEvent from 'model/event/DefaultEvent';

export const TYPE = "fuss";

export default class FussEvent extends DefaultEvent {

    /**
     * @param {Object} props Props as described in DefaultEvent
     * @param {Object} props.details The details for this event
     * @param {Number} props.details.severity A severity rating
     */
    constructor(props) {

        const {
            severity
        } = props.details || {};

        super(TYPE, true, props, { severity });

    }

}

FussEvent.type = TYPE;
