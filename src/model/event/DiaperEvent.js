import DefaultEvent from 'model/event/DefaultEvent';

export const TYPE = "diaper";

export const AMOUNTS = [
    'Barely anything',
    'A little bit',
    'Normal amount',
    'Quite a bit',
    'Blow out'
];

export const COLORS = [
    'Black (meconium)',
    'Yellow',
    'Green',
    'Brown'
];

export default class DiaperEvent extends DefaultEvent {

    /**
     * @param {Object} props Props as described in DefaultEvent
     * @param {Object} props.details The details for this event
     * @param {Boolean} props.details.dirty Dirty diaper (poop)
     * @param {Boolean} props.details.wet Wet diaper (pee)
     * @param {String} props.details.amount
     * @param {String} props.details.color
     */
    constructor(props) {

        const {
            dirty,
            wet,
            amount = 'Normal amount',
            color
        } = props.details || {};

        super(TYPE, false, props, { dirty, wet, amount, color });

    }

}

DiaperEvent.type = TYPE;
