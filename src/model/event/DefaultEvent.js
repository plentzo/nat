import md5 from 'md5';
import DateUtil from 'util/date';

export default class DefaultEvent {

    /**
     * @param {String} type The object's type (e.g. "eat")
     * @param {Boolean} timed Whether this event is timed (has a duration) or not
     * @param {Object} props
     * @param {Number} props.eventId
     * @param {Number} props.date Date in milliseconds
     * @param {Number} props.updated Updated date in milliseconds
     * @param {Number|Boolean} props.deleted The deleted date in milliseconds (false if not deleted)
     * @param {Boolean} props.starred Whether this event is starred with importance
     * @param {String} props.note A quick note for the event
     * @param {Number} props.duration If timed, the duration of the event. Zero if no duration set.
     * @param {Object} details Event-specific details (See any of the other event classes)
     */
    constructor(type, timed, props, details) {

        const {
            eventId,
            date = new Date(),
            updated = new Date(),
            deleted = false,
            starred = false,
            note,
            duration = null
        } = props;
        
        if (!type) {
            throw Error('Cannot create an event without a type');
        }

        this.eventId = eventId;
        this.type = type;
        this.date = DateUtil.timestamp(date);
        this.updated = DateUtil.timestamp(updated);
        this.deleted = deleted ? DateUtil.timestamp(deleted) : false;
        this.starred = starred;
        this.note = note;
        this.timed = timed;
        this.duration = duration;
        this.setDetails(details || {});
        
    }

    get hash() {
        const {
            eventId,
            type,
            date,
            deleted,
            starred,
            note,
            timed,
            duration,
            unsynced,
            details
        } = this;
        return md5(JSON.stringify({
            eventId,
            type,
            date,
            deleted,
            starred,
            note,
            timed,
            duration,
            unsynced,
            details
        }));
    }

    set hash(v) {
        throw Error('Hash cannot be set');
    }

    getEventId() {
        return this.eventId;
    }

    getType() {
        return this.type;
    }

    getIcon() {
        return '';
    }

    getDate() {
        return this.date;
    }

    getDateFormatted() {
        return DateUtil.formatDate(this.date);
    }

    getDateTimeFormatted() {
        return DateUtil.formatDateTime(this.date);
    }

    getTimeFormatted() {
        return DateUtil.formatTime(this.date);
    }

    getDurationFormatted() {
        return DateUtil.formatDuration(this.duration);
    }

    getLabel() {
        return 'Unknown Event';
    }

    getSummary() {
        return '';
    }

    getNote() {
        return this.note;
    }

    isActive() {
        return this.isTimed() && this.duration <= 0;
    }

    isTimed() {
        return this.timed;
    }

    setDetails(values) {
        this.details = {};
        Object.entries(values).forEach(([key, value]) => this.details[key] = value);
    }

}