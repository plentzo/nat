import DefaultEvent from 'model/event/DefaultEvent';

export const TYPE = "sleep";

export const LOCATIONS = [
    'Bassinet',
    'Crib',
    'Co-sleep',
    'Other'
];

export default class SleepEvent extends DefaultEvent {

    /**
     * @param {Object} props Props as described in DefaultEvent
     * @param {Object} props.details The details for this event
     * @param {String} props.details.location The location of the sleep
     */
    constructor(props) {

        const {
            location
        } = props.details || {};

        super(TYPE, true, props, { location });

    }

}

SleepEvent.type = TYPE;
