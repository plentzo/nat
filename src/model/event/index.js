import DefaultEvent from 'model/event/DefaultEvent';
import DiaperEvent from 'model/event/DiaperEvent';
import EatEvent from 'model/event/EatEvent';
import FussEvent from 'model/event/FussEvent';
import MessageEvent from 'model/event/MessageEvent';
import SleepEvent from 'model/event/SleepEvent';
import SizeEvent from 'model/event/SizeEvent';

export const typeClassMap = {
    DefaultEvent,
    DiaperEvent,
    EatEvent,
    FussEvent,
    MessageEvent,
    SleepEvent,
    SizeEvent
};

const typeClasses = Object.values(typeClassMap);

export const types = typeClasses.filter(tc => tc.type).map(tc => tc.type);

/**
 * @param {Object} props The props that will be passed to the appropriate constructor. This must have a type.
 */
export function create(props) {

    if ('undefined' === typeof props.type) {
        throw Error('Cannot create an event instance without a type');
    }

    const eventClass = typeClasses.find(typeClass => props.type === typeClass.type);

    if (!eventClass) {
        throw Error(`No event class found for type ${props.type}`);
    }

    return new eventClass(props);

}

/**
 * @param {Event|Array<Event>} events One or more events to convert into an object if necessary
 * @returns {Event|Array<Event>}
 */
export function objectify(event) {

    if (Array.isArray(event)) {
        // Multiple
        return event.map(objectify);
    } else {
        // Single
        if ('function' === typeof event.getType) {
            // It's already a class
            return event;
        } else {
            // Convert it
            return create(event);
        }
    }

}

export default {
    create,
    objectify
};

