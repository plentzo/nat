import md5 from 'md5';
import Event from 'model/event';
import DateUtil from 'util/date';

function createMd5HashForEventId(userId) {
    return md5([userId, new Date().getTime()].join('.')).substr(0, 8);
}

export function reduceToEventsMap(events) {
    return events.reduce((acc, event) => {
        acc[event.eventId] = event;
        return acc;
    }, {});
}

export function reduceToHashesMap(events) {
    return events.reduce((acc, event) => {
        acc[event.eventId] = event.hash;
        return acc;
    }, {});
}

export default class Events {

    constructor(events) {
        this.events = (events || []).map(Event.create);
    }

    get events() { return this._events }
    set events(value) { this._events = value; }

    get hash() {
        return md5(this.events.map(event => event.hash).join(','));
    }

    set hash(v) { throw Error('Hash cannot be set'); }

    add(userId, event) {

        if (Array.isArray(event) || (event instanceof Events)) {
            return event.map(e => this.add(userId, e));
        }

        const resolvedEvent = Event.create(event);

        // Guarantee an ID. Use a negative ID if one isn't provided; it will turn into a real
        // ID when we sync it with the server.
        resolvedEvent.eventId = resolvedEvent.eventId || this.getNextEventId(userId);

        this.events.push(resolvedEvent);

        return resolvedEvent;

    }

    find(...args) {
        return this.events.find(...args);
    }

    findIndex(...args) {
        return this.events.findIndex(...args);
    }

    filter(...args) {
        return this.events.filter(...args);
    }

    getNextEventId(userId) {
        if (!userId) {
            throw Error('Cannot create an event ID without a user ID');
        }
        let eventId = createMd5HashForEventId(userId);
        while (this.findIndex(event => event.eventId === eventId) !== -1) {
            eventId = createMd5HashForEventId(userId);
        }
        return eventId;
    }

    map(...args) {
        return this.events.map(...args);
    }

    nondeleted() {
        return this.filter(event => !event.deleted);
    }

    reduce(...args) {
        return this.events.reduce(...args);
    }

    remove(event) {

        if (Array.isArray(event) || (event instanceof Events)) {
            return event.map(e => this.remove(e));
        }

        const index = this.findIndex(e => event.eventId === e.eventId);

        if (index > -1) {
            this.events[index].updated = DateUtil.timestamp();
            this.events[index].deleted = this.events[index].updated;
        }

    }

    sort(...args) {
        return this.events.sort(...args);
    }

    toJSON() {
        return this.events;
    }

    update(event) {

        if (Array.isArray(event) || (event instanceof Events)) {
            return event.map(e => this.update(e));
        }

        if (event.eventId) {

            const index = this.findIndex(e => event.eventId === e.eventId);
            event.updated = DateUtil.timestamp();

            if (index > -1) {
                this.events[index] = Event.create(event);
                return this.events[index];
            } else {
                return this.add(null, event);
            }

        } else {
            throw Error('Cannot update an event without an eventId');
        }

    }

};
