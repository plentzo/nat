import 'babel-polyfill';
import Vue from 'vue';

// Store
import initializeStore from 'store';
const store = initializeStore(Vue);

// FIXME REMOVE
// import User from 'model/User';
// const testUser = new User({
//     userId: 1,
//     name: 'Test User',
//     email: 'test@plentz.net',
//     settings: {},
//     babies: [1]
// });
// store.commit('setUser', testUser);

// DAO
import initializeDao from 'dao';
const dao = initializeDao(Vue);

// Router
import initializeRouter from './router';
const router = initializeRouter(Vue, store, dao);

// Initial Sync
import { sync } from 'component/sync';
if (store.getters.user && store.getters.baby) {
    (async (store, dao) => await sync(store, dao))(store, dao);
}

// Service Workers
// if (!DEV && !PREPROD && 'serviceWorker' in navigator) {
//     navigator.serviceWorker.register('sw-cache.js');
// }

// Components
import components from 'component';
Vue.use(components);

// View Components (route-driven)
import viewComponents from 'view';
Vue.use(viewComponents, router);

// Vuetify
import Vuetify from 'vuetify';
import Theme from 'model/theme';
let theme = Theme.getPreset(store.state.settings.theme);
Vue.use(Vuetify, { theme: theme.colors });
require('vuetify/src/stylus/main.styl');

// App
import app from 'component/app.vue';

// Start
new Vue({
    el: '#app',
    render: h => h(app),
    router,
    store
});
