<?php

    require_once("../_mysqli.php");
    require_once("../_lib.php");

    echo "Initializing databases... ";

    query("
        INSERT INTO Users (email, name, settings) VALUES ('plentz@gmail.com', 'Patrick', '{}')
    ");

    $userId = $mysqli->insert_id;

    query("
        INSERT INTO Babies (name, gender) VALUES ('Nathaniel', 'boy')
    ");

    $babyId = $mysqli->insert_id;

    query("
        INSERT INTO UserBabies (userId, babyId) VALUES ($userId, $babyId)
    ");

    query("
        CREATE TABLE IF NOT EXISTS BabyEvents (
            eventId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            babyId INT NOT NULL,
            type VARCHAR(16) NOT NULL,
            date DATETIME NOT NULL,
            note VARCHAR(1024) NULL,
            duration INT NOT NULL DEFAULT 0,
            details VARCHAR(2048) NULL,
            FOREIGN KEY (babyId) REFERENCES Babies(babyId)
        ) ENGINE=INNODB;
    ");

    query("
        CREATE TABLE IF NOT EXISTS EventLog (
            logId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            babyId INT NOT NULL,
            eventId INT NOT NULL,
            date DATETIME NOT NULL,
            action ENUM('create', 'update', 'delete')
        ) ENGINE=INNODB;
    ");

    echo "Done";

	$mysqli->close();

?>