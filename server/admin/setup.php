<?php

    require_once("../_mysqli.php");
    require_once("../_lib.php");

    echo "Initializing databases... ";

    query("
        CREATE TABLE IF NOT EXISTS Users (
            userId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            email VARCHAR(256) NOT NULL,
            name VARCHAR(256) NULL,
            imageUrl VARCHAR(512) NULL,
            settings TEXT
        ) ENGINE=INNODB;
    ");

    query("
        CREATE TABLE IF NOT EXISTS Babies (
            babyId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(256) NOT NULL,
            gender VARCHAR(4) NOT NULL
        ) ENGINE=INNODB;
    ");

    query("
        CREATE TABLE IF NOT EXISTS UserBabies (
            userId INT NOT NULL,
            babyId INT NOT NULL,
            FOREIGN KEY (userId) REFERENCES Users(userId),
            FOREIGN KEY (babyId) REFERENCES Babies(babyId)
        ) ENGINE=INNODB;
    ");

    query("
        CREATE TABLE IF NOT EXISTS BabyEvents (
            babyId INT NOT NULL,
            date DATE NOT NULL,
            hash CHAR(32) NOT NULL,
            events MEDIUMTEXT NOT NULL,
            FOREIGN KEY (babyId) REFERENCES Babies(babyId)
        ) ENGINE=INNODB;
    ");

    echo "Done";

	$mysqli->close();

?>