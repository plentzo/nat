<?php

    require_once("../_mysqli.php");
    require_once("../_lib.php");

    query("SET FOREIGN_KEY_CHECKS = 0;
        DROP TABLE IF EXISTS Babies;
        DROP TABLE IF EXISTS Users;
        DROP TABLE IF EXISTS UserBabies;
        SET FOREIGN_KEY_CHECKS = 1;
    ");

    echo "Done";

	$mysqli->close();

?>