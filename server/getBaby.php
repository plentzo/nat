<?php

    require_once("_lib.php");
    require_once("Baby.php");

    $userId = (int) $_REQUEST["userId"];
    $babyId = (int) $_REQUEST["babyId"];

    // TODO verify token

    if ($userId) {
        
        $sql = "SELECT * FROM Babies WHERE babyId = $babyId";

        $babies_result = query($sql);
        $babies_row = $babies_result->fetch_assoc();

        $baby = new Baby(
            $babies_row["babyId"],
            $babies_row["name"],
            $babies_row["gender"]
        );

        header("Content-type: application/json");
        echo(json_encode($baby));

    }

?>