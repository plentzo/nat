<?php

    require_once("_lib.php");
    require_once("Event.php");

    $payload_json = json_decode(file_get_contents('php://input'));

    $userId = (int) $payload_json->userId;
    $babyId = (int) $payload_json->babyId;
    $hash = res($payload_json->hash);
    $events = res($payload_json->events);

    if ($userId && $babyId && $hash && $events) {

        $date = date('Y-m-d');

        // Determine if a row exists for today's date
        $sql = "SELECT Date FROM BabyEvents WHERE BabyId=$babyId AND date='$date'";
        $datecheck_result = query($sql);

        if ($datecheck_result->num_rows > 0) {
            // Exists
            $sql = "UPDATE BabyEvents SET Hash='$hash', Events='$events' WHERE BabyId=$babyId AND date='$date'";
        } else {
            // Didn't exist
            $sql = "INSERT INTO BabyEvents (BabyId, Date, Hash, Events) VALUES ($babyId, '$date', '$hash', '$events')";
        }

        // Update it
        query($sql);

        header("Content-type: application/json");
        echo(true);

    }

?>