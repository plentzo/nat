<?php

    require_once("_lib.php");
    require_once("Baby.php");

    $payload_json = json_decode(file_get_contents('php://input'));

    $userId = pres_int($payload_json, 'userId');
    $name = pres($payload_json->baby, 'name');
    $gender = pres($payload_json->baby, 'gender');

    if ($userId) {
        
        $sql = "INSERT INTO Babies (name, gender) VALUES ('$name', '$gender')";
        query($sql);
        $babyId = $mysqli->insert_id;

        $sql = "INSERT INTO UserBabies (userId, babyId) VALUES ($userId, $babyId)";
        query($sql);

        $sql = "SELECT * FROM Babies WHERE babyId = $babyId";
        $babies_result = query($sql);
        $babies_row = $babies_result->fetch_assoc();
        $baby = new Baby(
            $babies_row["babyId"],
            $babies_row["name"],
            $babies_row["gender"]
        );

        header("Content-type: application/json");
        echo(json_encode($baby));

    }

?>