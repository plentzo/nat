<?php

    class User {

        public $userId;
        public $name;
        public $email;
        public $settings;
        public $babies;

        function __construct($userId, $name, $email, $settings) {
            $this->userId = (int) $userId;
            $this->name = $name;
            $this->email = $email;
            $this->settings = $settings;
            $this->babies = [];
        }

    }

?>