<?php

    require_once("_lib.php");
    require_once("Baby.php");

    $payload_json = json_decode(file_get_contents('php://input'));

    $userId = pres_int($payload_json, 'userId');
    $settings = pres_json($payload_json, 'settings');

    if ($userId) {

        $sql = "UPDATE Users SET settings='$settings' WHERE UserId=$userId";
        query($sql);

        $sql = "SELECT settings FROM Users WHERE userId=$userId";
        $user_result = query($sql);
        $user_row = $user_result->fetch_assoc();
        $settings = $user_row['settings'];

        header("Content-type: application/json");
        echo($settings);

    }

?>