<?php

    require_once("_lib.php");

    $userId = (int) $_REQUEST["userId"];
    $babyId = (int) $_REQUEST["babyId"];

    header("Content-type: application/json");
    
    if ($userId && $babyId) {

        $sql = "SELECT hash FROM BabyEvents WHERE babyId = $babyId AND date = (SELECT MAX(date) FROM BabyEvents WHERE babyId = $babyId)";

        $events_result = query($sql);

        if ($events_result->num_rows > 0) {
            $events_row = $events_result->fetch_assoc();
            echo '"' . $events_row["hash"] . '"';
        } else {
            echo '""';
        }

    } else {
        echo '""';
    }

?>