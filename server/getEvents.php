<?php

    require_once("_lib.php");

    $userId = (int) $_REQUEST["userId"];
    $babyId = (int) $_REQUEST["babyId"];
    $events = [];

    
    if ($userId && $babyId) {

        $sql = "SELECT * FROM BabyEvents WHERE babyId = $babyId AND date = (SELECT MAX(date) FROM BabyEvents WHERE babyId = $babyId)";

        $events_result = query($sql);

        if ($events_result->num_rows > 0) {

            $events_row = $events_result->fetch_assoc();
            $events_base64_gz = $events_row["events"];
            $events_base64 = base64_decode($events_base64_gz);
            $events = gzdecode($events_base64);
            
            header("Content-type: application/json");
            $events_encoded = gzencode($events);
            header("Content-Encoding: gzip");
            header("Content-Length: " . strlen($events_encoded));
            header('Vary: Accept-Encoding');
            print($events_encoded);
            
        } else {
            header("Content-type: application/json");
            print(json_encode($events));
        }

    } else {
        header("Content-type: application/json");
        print(json_encode($events));
    }

?>