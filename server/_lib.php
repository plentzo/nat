<?php

    require_once("_mysqli.php");
    require_once("_var.php");
    require_once("_session.php");

    function query($sql) {
        global $mysqli;
        $result = $mysqli->query($sql);
        if (!$result) {
            die($mysqli->errno . ": " . $mysqli->error);
        } else {
            return $result;
        }
    }

    function res($obj) {
        global $mysqli;
        return $mysqli->real_escape_string($obj);
    }

    function pres($obj, $prop) {
        if (property_exists($obj, $prop)) {
            return res($obj->$prop);
        } else {
            return '';
        }
    }

    function pres_date($obj, $prop) {
        if (property_exists($obj, $prop)) {
            return date('Y-m-d\TH:i:s', strtotime($obj->$prop));
        } else {
            return null;
        }
    }

    function pres_int($obj, $prop) {
        if (property_exists($obj, $prop)) {
            return (int) $obj->$prop;
        } else {
            return 0;
        }
    }

    function pres_json($obj, $prop) {
        if (property_exists($obj, $prop)) {
            return res(json_encode($obj->$prop));
        } else {
            return (object)[];
        }
    }

    function verify_token($token) {

        global $_SESSION;

        $separator = ".";

        if (2 !== substr_count($token, $separator)) {
            throw new Exception("Incorrect access token format");
        }

        list($header, $payload, $signature) = explode($separator, $token);

        $header_json = json_decode(base64_decode($header));
        $payload_json = json_decode(base64_decode($payload));

        $session_key = "token_" . $token;

        // if ($_SESSION["$session_key"] == true) {
        //     return $payload_json;
        // }

        $passes = true;

        // Verify it matches google's public key
        $google_auth_certs = json_decode(file_get_contents("https://www.googleapis.com/oauth2/v3/certs"));
        $passes_cert = false;
        foreach($google_auth_certs->keys as $key) {
            $passes_cert = $passes_cert || ($header_json->alg == $key->alg && $header_json->kid == $key->kid);
        }
        $passes = $passes && $passes_cert;

        // Verify our client ID
        $passes = $passes && ($payload_json->aud == CLIENT_ID);

        // Verify the google accounts address
        $passes = $passes && ($payload_json->iss == "accounts.google.com" || $payload_json->iss == "https://accounts.google.com");

        // Verify that the token hasn't expired
        $passes = $passes && (time() < $payload_json->exp);

        // Store results in the session
        // $_SESSION[$session_key] = $passes;

        if ($passes) {
            return $payload_json;
        } else {
            return false;
        }

    }

    function gzip_to_string($obj_base64_gz) {
        $obj_base64 = base64_decode($obj_base64_gz);
        $obj = gzdecode($obj_base64);
        return $obj;
    }
    
    function convertResultSetToEvents($events_result) {

        $events = [];

        while ($events_row = $events_result->fetch_assoc()) {
            $events[] = new Event(
                $events_row["eventId"],
                $events_row["type"],
                $events_row["date"],
                $events_row["note"],
                $events_row["duration"],
                $events_row["details"]
            );
        }

        return $events;

    }

?>