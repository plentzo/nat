<?php

    require_once("_lib.php");
    require_once("User.php");

    function authorize_user($token) {

        global $mysqli;

        $token_json = verify_token($token);

        if (false === $token_json) {
            die("Invalid user");
            echo $token_json->email;
        }

        // See if the user exists
        $user_result = query("SELECT * FROM Users WHERE email = '" . res($token_json->email) . "'");
        
        if ($user_result->num_rows === 0) {
            query("INSERT INTO Users (email, name, settings) VALUES ('" . res($token_json->email) . "', '" . res($token_json->name) . "', '{}')");
            $user_result = query("SELECT * FROM Users WHERE userId='$mysqli->insert_id'");
        }

        $user_row = $user_result->fetch_assoc();

        $user = new User($user_row["userId"], $user_row["name"], $user_row["email"], $user_row["settings"]);

        // Add the user's baby IDs
        $babies_result = query("SELECT babyId FROM UserBabies WHERE UserId=" . $user_row["userId"]);

        while ($babies_row = $babies_result->fetch_assoc()) {
            $user->babies[] = $babies_row["babyId"];
        }

        return json_encode($user);

    }

    if ($_REQUEST["token"]) {
        // We're getting a user from their token
        header("Content-type: application/json");
        echo(authorize_user($_REQUEST["token"]));
    }

?>