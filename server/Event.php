<?php

    class Event {

        public $eventId;
        public $type;
        public $date;
        public $note;
        public $duration;
        public $details;

        function __construct($eventId, $type, $date, $note, $duration, $details) {
            $this->eventId = $eventId;
            $this->type = $type;
            $this->date = (int) ($date / 1000);
            $this->note = $note;
            $this->duration = (int) $duration;
            $this->details = $details;
        }

        function get_duration_str() {

            if (property_exists($this, 'duration') && $this->duration > 0) {

                $hours = floor($this->duration / 3600);
                $minutes = ceil(($this->duration / 60) % 60);
                $parts = [];

                if ($hours) {
                    $parts[] = $hours . "hr";
                }

                if ($minutes) {
                    $parts[] = $minutes . "min";
                }

                return implode(' ', $parts);

            } else {

                return '';

            }

        }

        function get_label() {

            switch ($this->type) {

                /* Diaper */
                case "diaper":
                    return "Diaper Change";

                /* Eat */
                case "eat":
                    if (property_exists($this, 'details') && property_exists($this->details, 'type')) {
                        switch ($this->details->type) {
                            case "breast":
                                return "Breast Feeding";
                            case "bottle":
                                return "Bottle";
                            case "pump":
                                return "Pump";
                            default:
                                return "Feeding";
                        }
                    } else {
                        return "Eat";
                    }

                /* Fuss */
                case "fuss":
                    return "Fuss";

                /* Message */
                case "message":
                    return "Message";

                /* Size */
                case "size":
                    if (property_exists($this, 'details')) {
                        if (property_exists($this->details, 'height')) {
                            if (property_exists($this->details, 'weight')) {
                                return 'Height/Weight';
                            } else {
                                return 'Height';
                            }
                        } else if (property_exists($this->details, 'weight')) {
                            return 'Weight';
                        } else {
                            return 'Size';
                        }
                    } else {
                        return "Size";
                    }

                /* Sleep */
                case "sleep":
                    return "Sleep";

                /* Default */
                default:
                    return $this->type;
                
            }

        }

        function get_summary() {

            $details = $this->details;

            switch ($this->type) {

                /* Diaper */
                case "diaper":
                    $types = [];
                    if (property_exists($details, "wet") && $details->wet) {
                        $types[] = 'Wet';
                    }
                    if (property_exists($details, "dirty") && $details->dirty) {
                        $dirtyTypes = [];
                        if (property_exists($details, "color")) {
                            $dirtyTypes[] = $details->color;
                        }
                        if (property_exists($details, "amount")) {
                            $dirtyTypes[] = $details->amount;
                        }
                        if ($dirtyTypes) {
                            $types[] = 'Dirty ' . implode(' ', $dirtyTypes);
                        } else {
                            $types[] = 'Dirty';
                        }
                    }
                    return implode(' ', $types);

                /* Eat */
                case "eat":
                    if (property_exists($details, 'type')) {
                        switch ($details->type) {
                            case "breast":
                                return (property_exists($details, 'breast') ? ucwords($details->breast) : '');
                            case "bottle":
                                return (property_exists($details, 'amount') ? ucwords($details->amount) : '');
                            case "pump":
                                return implode(" ", [
                                    property_exists($details, 'breast') ? ucwords($details->breast) : '',
                                    property_exists($details, 'amount') ? ucwords($details->amount) : ''
                                ]);
                        }
                    }
                    break; 

                /* Fuss */
                case "fuss":
                    if (property_exists($details, 'severity')) {
                        return "Level " . $details->severity;
                    }
                    break;

                /* Message */
                case "message":
                    return '';

                /* Size */
                case "size":
                    $parts = [];
                    if (property_exists($details, 'height')) {
                        $feet = floor($details->height / 12);
                        $inches = $details->height - $feet * 12;
                        $height_parts = [];
                        if ($feet > 0) $height_parts[] = $feet . "'";
                        if ($inches > 0) $height_parts[] = $inches . "\"";
                        $parts[] = implode(" ", $height_parts);
                    }
                    if (property_exists($details, 'weight')) {
                        $pounds = floor($details->weight / 16);
                        $ounces = $details->weight - $pounds * 16;
                        $weight_parts = [];
                        if ($pounds > 0) $weight_parts[] = $pounds . "lb";
                        if ($ounces > 0) $weight_parts[] = $ounces . "oz";
                        $parts[] = implode(" ", $weight_parts);
                    }
                    return implode(', ', $parts);

                /* Sleep */
                case "sleep":
                    return $details->location;
                
            }

            return json_encode($details);

        }

    }

?>