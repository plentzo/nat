<?php

    require_once("_lib.php");
    require_once("Event.php");

    function csv_escape($str) {
        return "\"" . str_replace("\"", "\"\"", $str) . "\"";
    }

    $user_id = res($_REQUEST["userId"]);
    $baby_id = res($_REQUEST["babyId"]);
    $type = $_REQUEST["type"];

    $separator = ",";

    $baby_row = query("SELECT * FROM Babies WHERE babyId=$baby_id")->fetch_assoc();
    $events_row = query("SELECT * FROM BabyEvents WHERE babyId=$baby_id ORDER BY date DESC LIMIT 1")->fetch_assoc();

    $baby_name = res($baby_row["name"]);
    $events = json_decode(gzip_to_string($events_row["events"]));

    $filename = $baby_name . "_" . date("Y-m-d") . "_" . date("H-i");

    if ($user_id && $baby_id && $type) {

        if ('json' == $type) {

            header("Content-Disposition: attachment;filename=$filename.json");

            print json_encode($events);

        } else if ('csv' == $type) {

            header("Content-Disposition: attachment;filename=$filename.csv");

            print implode($separator, [ "Date", "Time", "Event", "Duration", "Details", "Note" ]) . "\n";

            foreach ($events as $event_arr) {
                $event = new Event(
                    $event_arr->eventId,
                    $event_arr->type,
                    $event_arr->date,
                    property_exists($event_arr, 'note') ? $event_arr->note : '',
                    property_exists($event_arr, 'duration') ? $event_arr->duration : 0,
                    $event_arr->details
                );
                print implode($separator, [
                    date('m/d/Y', $event->date),
                    date('H:i', $event->date),
                    csv_escape($event->get_label()),
                    csv_escape($event->get_duration_str()),
                    csv_escape($event->get_summary()),
                    csv_escape($event->note)
                ]) . "\n";
            }

        }

    }

?>