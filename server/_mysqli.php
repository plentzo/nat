<?php

    require_once("_var.php");

    $mysqli = new mysqli(
        NAT_DB_HOSTNAME,
        NAT_DB_USERNAME,
        NAT_DB_PASSWORD,
        NAT_DB_DATABASE
    );

	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }
    
?>