<?php

    class Baby {

        public $babyId;
        public $name;
        public $gender;

        function __construct($babyId, $name, $gender) {
            $this->babyId = (int) $babyId;
            $this->name = $name;
            $this->gender = $gender;
        }

    }

?>